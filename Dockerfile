FROM ubuntu

MAINTAINER https://gitlab.com/dedmin

ENV BIND_USER=bind \
    BIND_CONF_DIR=/etc/bind \
    BIND_LOG_DIR=/var/log/named \
    BIND_SLAVE_DIR=/etc/bind/slave \
    BIND_MASTER_DIR=/etc/bind/master \
    BIND_PID_DIR=/var/run/named \
    TZ=Europe/Moscow

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install --no-install-recommends -y bind9 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY conf/ "$BIND_CONF_DIR/"
COPY entrypoint.sh /usr/local/bin

RUN cd "$BIND_CONF_DIR" && \
    chmod 644 db.* named.* && \
    mkdir "$BIND_SLAVE_DIR" "$BIND_PID_DIR" && \
    chown "$BIND_USER:$BIND_USER" "$BIND_SLAVE_DIR" && \
    chmod 755 /usr/local/bin/entrypoint.sh 

EXPOSE 53/udp 53/tcp

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
