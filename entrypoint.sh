#!/bin/bash

# Entrypoint for docker bind container

BIND=`which named`

create_dir() {
	mkdir -p "$BIND_PID_DIR" && \
	chown -R "$BIND_USER:$BIND_USER" "$BIND_PID_DIR"
}

perm_files() {
	chmod 644 "$BIND_CONF_DIR"/db.* "$BIND_CONF_DIR"/named.* && \
	chmod 640 "$BIND_CONF_DIR"/*.key && \
	chown -R "$BIND_USER:$BIND_USER" "$BIND_SLAVE_DIR" "$BIND_PID_DIR" "$BIND_CONF_DIR"/*.key
}


perm_files
echo "start bind"
$BIND -u $BIND_USER -f