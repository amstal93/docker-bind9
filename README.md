# Docker-bind9


[![Pipeline Status](https://gitlab.com/dedmin/bind9/badges/master/pipeline.svg)](https://gitlab.com/dedmin/bind9/commits/master) 

# Introduction

This is automated build of bind for runing in docker container


# Bind9

BIND is open source software that implements the Domain Name System (DNS) protocols for the Internet which provides ability to perform name to ip conversion. The name BIND stands for “Berkeley Internet Name Domain”, because the software originated in the early 1980s at the University of California at Berkeley. It is a reference implementation of DNS protocols, but it is also production-grade software, suitable for use in high-volume and high-reliability applications.

BIND is by far the most widely used DNS software on the Internet, providing a robust and stable platform on top of which organizations can build distributed computing systems with the knowledge that those systems are fully compliant with published DNS standards.

# Building

Run it in command line:

```bash
git clone https://gitlab.com/dedmin/bind9.git
cd bind9
docker build . -t dedmin/bind9
```


# Configuration

Run container on the host system and mount directories and configuration file:

```bash
wget 'https://gitlab.com/dedmin/bind9/raw/master/bind-run.sh'
bash bind-run.sh
or
docker pull dedmin/bind9
docker run -d --name bind9 --restart always dedmin/bind9
```