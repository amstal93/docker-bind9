#!/bin/bash

BIND_USER=bind
BIND_CONF_DIR=/etc/bind
BIND_LOG_DIR=/var/log/named
BIND_SLAVE_DIR=/etc/bind/slave
BIND_MASTER_DIR=/etc/bind/master
BIND_PID_DIR=/var/run/named

create_dir() {
	mkdir -p ./conf/slave ./conf/master
}


if [ ! -d "./conf/slave" ] || [ ! -d "./conf/master" ]
then
	echo "create directories"
	create_dir
fi

echo "start bind9"

docker run -d --net host --name bind9 --restart always \
	-v "/etc/localtime:/etc/localtime:ro" \
	-v "/etc/timezone:/etc/timezone:ro" \
	-v "$(pwd)/conf/master:$BIND_MASTER_DIR:ro" \
	-v "$(pwd)/conf/slave:$BIND_SLAVE_DIR:rw" \
	-v "$(pwd)/conf/named.conf.options:$BIND_CONF_DIR/named.conf.options:ro" \
	dedmin/bind9:latest

